<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//test
Route::get('/coba/test/testing', 'UserController@coba');
Route::get('/schedule/jabar/to/db', 'UserController@storeJabarToDb');
Route::get('/schedule/notif/twice', 'UserController@notif');
//test


Route::get('/national', 'NasionalController@all_national');

//contoh -> /nation/date?date=1588032000000
Route::get('/national/date', 'NasionalController@home');

Route::get('/provinces', 'ProvinsiController@home');

//contoh -> /province?region_name=nama_provinsi_kabko
Route::get('/province', 'ProvinsiController@perProvinsi');

//contoh -> /register?device_id=0&location=xxx&app_token=app_token
Route::post('/register', 'UserController@register');

//khusus jawa barat -> /api/jawa_barat?district=xxx&sub_dist=xxxx
Route::get('/jawa_barat', 'JawaController@jawa_barat');

//Khusus jawa timur -> /api/jawa_timur?district=xxx
Route::get('jawa_timur', 'JawaController@jawa_timur');

Route::get('aceh', 'SumatraController@acehAll');
Route::get('kalsel', 'KalimantanController@kalselAll');
Route::get('sulsel', 'SulawesiController@sulselAll');
// api/banten?district=xxx
Route::get('banten', 'JawaController@bantenAll');

// api/sumbar?district=xxx
Route::get('sumbar', 'SumatraController@sumbarAll');
