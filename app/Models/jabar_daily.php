<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class jabar_daily extends Model
{
    protected $table = "jabarprov_sebaran_daily";
    protected $fillable = ['data'];
    public $timestamps = false;
}
