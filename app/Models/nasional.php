<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class nasional extends Model
{
    protected $table = "nasional";
    protected $fillable = ["hari_ke","investigasi_lapangan","kabkota_terdampak"];
}
