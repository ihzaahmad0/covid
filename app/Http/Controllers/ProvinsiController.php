<?php

namespace App\Http\Controllers;

use App\Models\jabar_daily;
use App\Models\jabarprovsebaran;
use App\Models\provinsi;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProvinsiController extends Controller
{
    // tanggal urut dari kecil ke besar
    // public function home()
    // {
    //     $db = provinsi::orderBy('id', 'desc')->take(34)->get(['tanggal', 'wilayah', 'total_meninggal', 'total_sembuh']);
    //     $data["provinces"] = array();
    //     $i = 0;
    //     for ($j = 33; $j >= 0; $j--) {
    //         $data["provinces"][$i]["date"] = $db[$j]->tanggal;
    //         $data["provinces"][$i]["name"] = $db[$j]->wilayah;

    //         $temp = provinsi::where('wilayah', $db[$j]->wilayah)->orderBy('id', 'desc')->take(8)->get(['total_positif', 'tanggal'])->toArray();
    //         for ($k = 0; $k < 8; $k++) {
    //             $epoch = substr($temp[$k]['tanggal'], 0, 10);
    //             $dt = new DateTime("@$epoch");
    //             $temp[$k]['tanggal'] = $dt->format('d M');
    //         }
    //         $data["provinces"][$i]["total"] = array();
    //         for ($k = 7; $k > 0; $k--) {
    //             array_push($data["provinces"][$i]["total"], array($temp[$k]["tanggal"] => $temp[$k]["total_positif"]));
    //         }
    //         $data["provinces"][$i]["recovered"] = $db[$j]->total_sembuh;
    //         $data["provinces"][$i]["deceased"] = $db[$j]->total_meninggal;
    //         $data["provinces"][$i]["active"] = 0;
    //         $i++;
    //     }

    //     usort($data["provinces"], function ($a, $b) {
    //         return $a['name'] <=> $b['name'];
    //     });
    //     return response()->json($data);
    // }

    //tanggal dari besar ke kecil
    public function home()
    {
        $db = provinsi::orderBy('tanggal', 'asc')->groupBy('wilayah')->take(34)->get(['tanggal', 'wilayah', 'total_meninggal', 'total_sembuh']);
        $data["provinces"] = array();
        $i = 0;
        for ($j = 33; $j >= 0; $j--) {
            $data["provinces"][$i]["date"] = $db[$j]->tanggal;
            $data["provinces"][$i]["name"] = $db[$j]->wilayah;

            $temp = provinsi::where('wilayah', $db[$j]->wilayah)->orderBy('tanggal', 'desc')->take(8)->get(['total_positif', 'tanggal'])->toArray();
            for ($k = 0; $k < 8; $k++) {
                $epoch = substr($temp[$k]['tanggal'], 0, 10);
                $dt = new DateTime("@$epoch");
                $temp[$k]['tanggal'] = $dt->format('d M');
            }
            $data["provinces"][$i]["total"] = array();
            for ($k = 6; $k >= 0; $k--) {
                array_push($data["provinces"][$i]["total"], array($temp[$k]["tanggal"] => $temp[$k]["total_positif"]));
            }
            $data["provinces"][$i]["recovered"] = $db[$j]->total_sembuh;
            $data["provinces"][$i]["deceased"] = $db[$j]->total_meninggal;
            $data["provinces"][$i]["active"] = 0;
            $i++;
        }

        usort($data["provinces"], function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });
        return response()->json($data);
    }

    public function perProvinsi(Request $request)
    {
        try {
            $cnt = DB::table($request->region_name)
                ->groupBy('wilayah')
                ->get();
            $cnt = count($cnt);
            $db =
                DB::table($request->region_name)
                ->latest('id')
                ->take($cnt)
                ->get()
                ->toArray();
        } catch (Exception $e) {
            $cnt = DB::table($request->region_name)
                ->groupBy('nama_kab')
                ->get();
            $cnt = count($cnt);
            $db =
                DB::table($request->region_name)
                ->orderBy('id', 'desc')
                ->take($cnt)
                ->get()
                ->toArray();
        }
        $arr = json_decode(json_encode($db[0]), true);
        $idx =  array_keys($arr);

        $data['total_odp'] = 0;
        $data['total_pdp'] = 0;
        $data['total_positive'] = 0;
        $data['total_recovered'] = 0;
        $data['total_deceased'] = 0;
        $data['last_updated'] = $db[0]->tanggal;
        $odp = $idx[1];
        $pdp = $idx[2];
        $positif = $idx[3];
        $recov = $idx[4];
        $die = $idx[5];
        $tgl = $idx[6];
        $nama = $idx[7];
        $tidx = array();
        for ($i = 0; $i < count($db); $i++) {
            $wil = $db[$i]->$nama;

            for ($j = $i + 1; $j < count($db); $j++) {
                if ($j == count($db)) {
                    continue;
                } else {
                    if ($db[$j]->$nama == $wil) {
                        array_push($tidx, $j);
                    }
                }
            }
        }
        foreach ($tidx as $t) {
            unset($db[$t]);
        }
        $data['districts'] = array();
        foreach ($db as $d) {
            $data['total_odp'] += $d->$odp;
            $data['total_pdp'] +=  $d->$pdp;
            $data['total_positive'] +=  $d->$positif;
            $data['total_recovered'] +=  $d->$recov;
            $data['total_deceased'] +=  $d->$die;

            $tmp['name'] = $d->$nama;
            $tmp['total_odp'] = $d->$odp;
            $tmp['total_pdp'] = $d->$pdp;
            $tmp['total_positive'] = $d->$positif;
            $tmp['total_recovered'] = $d->$recov;
            $tmp['total_deceased'] = $d->$die;
            $tmp['last_updated'] = $d->$tgl;
            array_push($data['districts'], $tmp);
        }

        return response()->json($data);
    }
}
