<?php

namespace App\Http\Controllers;

use App\Models\aceh;
use App\Models\sumbar;
use App\Models\sumbar_kabko;
use Illuminate\Http\Request;

class SumatraController extends Controller
{
    public function acehAll()
    {
        $db = aceh::get();
        $data = array(
            "name" => "Aceh",
            "odp" => [],
            "pdp" => [],
            "pdp_in_care" => [],
            "pdp_recovered" => [],
            "positive" => [],
            "deceased" => [],
            "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal))) . '000'
        );

        foreach ($db as $d) {
            $tgl = date('d M', strtotime($d->tanggal));
            $data['odp'] += array($tgl => $d->total_odp);
            $data['pdp'] += array($tgl => $d->total_pdp);
            $data['pdp_in_care'] += array($tgl => $d->pdp_rawat);
            $data['pdp_recovered'] += array($tgl => $d->pdp_sembuh);
            $data['positive'] += array($tgl => $d->total_positif);
            $data['deceased'] += array($tgl => $d->pos_meninggal);
        }

        return response()->json($data);
    }

    public function sumbarAll(Request $request)
    {
        $district = $request->district;
        if ($district == "-1") {
            $db = sumbar::take(1)->orderBy('tanggal', 'desc')->get();
            $data = array(
                "name" => "Sumatra Barat",
                "odp" => [],
                "pdp" => [],
                "positive" => [],
                "last_updated" => $db[0]->tanggal,
                "districts" => []
            );
            foreach ($db as $d) {
                $tgl = date('d M', strtotime($d->tanggal));
                $data['odp'] += array(
                    "total" => $d->total_odp,
                    "process" => $d->odp_proses,
                    "finished" => $d->odp_selesai

                );
                $data['pdp'] += array(
                    "total" => $d->total_pdp,
                    "in_care" => $d->pdp_rawat,
                    "suspect" => $d->pdp_suspect,
                    "isolation" => $d->pdp_isolasi

                );
                $data['positive'] += array(
                    "total" => $d->total_positif,
                    "recovered" => $d->pos_sembuh,
                    "deceased" => $d->pos_meninggal,
                    "in_care" => $d->pos_rawat,
                    "self_isolation" => $d->pos_isolasi_mandiri
                );
            }
            $arr = sumbar_kabko::groupBy('kode_kab')->orderBy('wilayah')->pluck('kode_kab');
            $cnt = count($arr);
            $db = sumbar_kabko::whereIn('kode_kab', $arr)->take($cnt)->orderBy('tanggal', 'desc')->distinct('kode_kab')->get()->toArray();

            usort($db, function ($first, $second) {
                return strtolower($first['wilayah']) > strtolower($second['wilayah']);
            });
            foreach ($db as $d) {
                array_push($data['districts'], array(
                    "name" => $d['wilayah'],
                    "distric_code" => $d['kode_kab'],
                    "total_odp" => $d['total_odp'],
                    "total_pdp" => $d['total_pdp'],
                    "total_positive" => $d['total_positif'],
                    "total_recovered" => $d['total_sembuh'],
                    "total_deceased" => $d['total_meninggal'],
                    "last_updated" => strtotime(date('Y-m-d', strtotime($d['tanggal'])))
                ));
            }
        } else {
            $db = sumbar_kabko::where('kode_kab', $district)->orderBy('tanggal')->get();
            $data = array(
                "name" => $db[0]->wilayah,
                "odp" => [],
                "pdp" => [],
                "positive" => [],
                "recovered" => [],
                "deceased" => [],
                "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal)))
            );

            foreach ($db as $d) {
                $tgl = date('d M', strtotime($d->tanggal));
                $data['odp'] += array(
                    $tgl => $d->total_odp
                );
                $data['pdp'] += array(
                    $tgl => $d->total_pdp
                );
                $data['positive'] += array(
                    $tgl => $d->total_positif
                );
                $data['recovered'] += array(
                    $tgl => $d->total_sembuh
                );
                $data['deceased'] += array(
                    $tgl => $d->total_meninggal
                );
            }
        }

        return response()->json($data);
    }
}
