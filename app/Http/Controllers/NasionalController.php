<?php

namespace App\Http\Controllers;

use App\Models\jabar_daily;
use App\Models\nasional;
use DateTime;
use Illuminate\Http\Request;

class NasionalController extends Controller
{
    public function home(Request $request)
    {
        $curr = nasional::orderBy('hari_ke', 'desc')->first();
        $db = nasional::where('tanggal', '<=', $request->date)->orderBy('hari_ke', 'desc')->take(8)->get();
        $data["positive"]["total"] = $curr->total_positif;
        $data["positive"]["newest"] = array();
        $data["in_care"]["total"] = $curr->total_dirawat;
        $data["in_care"]["newest"] = array();

        $data["deceased"]["total"] = $curr->total_meninggal;
        $data["deceased"]["newest"] = array();
        $data["recovered"]["total"] = $curr->total_sembuh;
        $data["recovered"]["newest"] = array();
        $data["realtime_pcr"]["num_of_specimens_checked"] = $curr->total_spesimen_diperiksa;
        $data["realtime_pcr"]["sample_specimens_checked"]["total"] = $curr->kasus_diperiksa_spesimen;
        $data["realtime_pcr"]["sample_specimens_checked"]["new"] = $curr->kasus_diperiksa_spesimen_baru;
        $data["realtime_pcr"]["negative"] = $curr->kasus_diperiksa_spesimen_negatif;
        $data["total_odp"] = $curr->total_odp;
        $data["total_pdp"] = $curr->total_pdp;
        if (count($db) == 0) {
            $data["in_care"]["percentage_increase"] = 0;
            $data["positive"]["newest"] = 0;
            $data["in_care"]["newest"] = 0;
            $data["deceased"]["newest"] = 0;
            $data["recovered"]["newest"] = 0;
        } else if (count($db) == 1) {


            $data["in_care"]["percentage_increase"] = $db[0]->persen_dirawat;
            $epoch = substr($db[0]['tanggal'], 0, 10);
            $dt = new DateTime("@$epoch");
            $db[0]['tanggal'] = $dt->format('d M');
            array_push($data["positive"]["newest"], array($db[0]->tanggal => ($db[0]->total_positif)));
            array_push($data["in_care"]["newest"], array($db[0]->tanggal => ($db[0]->total_dirawat)));
            array_push($data["deceased"]["newest"], array($db[0]->tanggal => ($db[0]->total_meninggal)));
            array_push($data["recovered"]["newest"], array($db[0]->tanggal => ($db[0]->total_sembuh)));
        } else if (count($db) == 8) {
            $data["in_care"]["percentage_increase"] = $db[0]->persen_dirawat - $db[1]->persen_dirawat;
            for ($k = 0; $k < count($db); $k++) {
                $epoch = substr($db[$k]['tanggal'], 0, 10);
                $dt = new DateTime("@$epoch");
                $db[$k]['tanggal'] = $dt->format('d M');
            }
            for ($i = count($db) - 2; $i >= 0; $i--) {
                array_push($data["positive"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_positif - $db[$i + 1]->total_positif)));
                array_push($data["in_care"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_dirawat - $db[$i + 1]->total_dirawat)));
                array_push($data["deceased"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_meninggal - $db[$i + 1]->total_meninggal)));
                array_push($data["recovered"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_sembuh - $db[$i + 1]->total_sembuh)));
            }
        } else {
            $data["in_care"]["percentage_increase"] = $db[0]->persen_dirawat - $db[1]->persen_dirawat;
            for ($k = 0; $k < count($db); $k++) {
                $epoch = substr($db[$k]['tanggal'], 0, 10);
                $dt = new DateTime("@$epoch");
                $db[$k]['tanggal'] = $dt->format('d M');
            }
            array_push($data["positive"]["newest"], array($db[count($db) - 1]->tanggal => ($db[count($db) - 1]->total_positif)));
            array_push($data["in_care"]["newest"], array($db[count($db) - 1]->tanggal => ($db[count($db) - 1]->total_dirawat)));
            array_push($data["deceased"]["newest"], array($db[count($db) - 1]->tanggal => ($db[count($db) - 1]->total_meninggal)));
            array_push($data["recovered"]["newest"], array($db[count($db) - 1]->tanggal => ($db[count($db) - 1]->total_sembuh)));
            for ($i = count($db) - 2; $i >= 0; $i--) {
                array_push($data["positive"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_positif - $db[$i + 1]->total_positif)));
                array_push($data["in_care"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_dirawat - $db[$i + 1]->total_dirawat)));
                array_push($data["deceased"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_meninggal - $db[$i + 1]->total_meninggal)));
                array_push($data["recovered"]["newest"], array($db[$i]->tanggal => ($db[$i]->total_sembuh - $db[$i + 1]->total_sembuh)));
            }
        }
        return response()->json($data);
    }

    public function all_national()
    {
        $db = nasional::orderBy('tanggal')->get(['tanggal', 'total_positif', 'total_dirawat', 'total_meninggal', 'total_sembuh']);
        $data['positive'] = array();
        $data['in_care'] = array();
        $data['deceased'] = array();
        $data['recovered'] = array();
        foreach ($db as $d) {
            $epoch = substr($d['tanggal'], 0, 10);
            $dt = new DateTime("@$epoch");
            $d['tanggal'] = $dt->format('d M');
            $data['positive'] += array($d['tanggal'] => $d['total_positif']);
            $data['in_care'] += array($d['tanggal'] => $d['total_dirawat']);
            $data['deceased'] += array($d['tanggal'] => $d['total_meninggal']);
            $data['recovered'] += array($d['tanggal'] => $d['total_sembuh']);
        }
        return response()->json($data);
    }
}
