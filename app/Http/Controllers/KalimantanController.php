<?php

namespace App\Http\Controllers;

use App\Models\kalsel;
use Illuminate\Http\Request;

class KalimantanController extends Controller
{
    public function kalselAll()
    {
        $db = kalsel::orderBy('tanggal')->get();
        $data = array(
            "name" => "Kalimantan Selatan",
            "odp" => [],
            "pdp" => [],
            "positive" => [],
            "recovered" => [],
            "deceased" => [],
            "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal))) . '000'
        );

        foreach ($db as $d) {
            $tgl = date('d M', strtotime($d->tanggal));
            $data["odp"] += array(
                $tgl => $d->total_odp
            );
            $data["pdp"] += array(
                $tgl => $d->total_pdp
            );
            $data["positive"] += array(
                $tgl => $d->total_positif
            );
            $data["recovered"] += array(
                $tgl => $d->total_sembuh
            );
            $data["deceased"] += array(
                $tgl => $d->total_meninggal
            );
        }
        return response()->json($data);
    }
}
