<?php

namespace App\Http\Controllers;

use App\Models\sulsel;
use Illuminate\Http\Request;

class SulawesiController extends Controller
{
    public function sulselAll()
    {
        $db = sulsel::orderBy('tanggal')->get();
        $data = array(
            "name" => "Sulawesi Selatan",
            "odp" => [],
            "pdp" => [],
            "positive" => [],
            "in_care" => [],
            "recovered" => [],
            "deceased" => [],
            "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal))) . '000'
        );

        foreach ($db as $d) {
            $tgl = date('d M', strtotime($d->tanggal));
            $data['odp'] += array(
                $tgl => [
                    "total" => $d->total_odp,
                    "process" => $d->odp_proses,
                    "finish" => $d->odp_selesai
                ]
            );
            $data['pdp'] += array(
                $tgl => [
                    "total" => $d->total_pdp,
                    "deceased" => $d->pdp_meninggal,
                    "in_care" => $d->pdp_dirawat,
                    "recovered" => $d->pdp_sembuh
                ]
            );
            $data['positive'] += array(
                $tgl => [
                    "total" => $d->total_positif,
                    "deceased" => $d->pos_meninggal,
                    "in_care" => $d->pos_dirawat,
                    "recovered" => $d->pos_sembuh
                ]
            );
            $data['in_care'] += array(
                $tgl => $d->total_dirawat
            );
            $data['recovered'] += array(
                $tgl => $d->total_sembuh
            );
            $data['deceased'] += array(
                $tgl => $d->total_meninggal
            );
        }

        return response()->json($data);
    }
}
