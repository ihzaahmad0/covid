<?php

namespace App\Http\Controllers;

use App\Models\banten;
use App\Models\banten_kabko;
use App\Models\jabar_daily;
use App\Models\jabarprovsebaran;
use App\Models\Jatim;
use Illuminate\Http\Request;

class JawaController extends Controller
{
    public function jawa_timur(Request $request)
    {
        $district = $request->district;
        $data['name'] = "Jawa Timur";
        $data['odp'] = array();
        $data['pdp'] = array();
        $data['positive'] = array();
        $data['recovered'] = array();
        $data['deceased'] = array();
        $data['active'] = 0;
        if ($district == "-1") {
            // $db = provinsi::whereWilayah('Jawa Timur')->get(['tanggal', 'total_positif', 'total_sembuh', 'total_meninggal']);
            $jatim = Jatim::orderBy('tanggal')->get();
            foreach ($jatim as $j) {
                $j->tanggal = date('d M', strtotime($j->tanggal));
                if (array_key_exists($j->tanggal, $data['odp'])) {
                    $data['odp'][$j->tanggal] += $j->total_odp;
                    $data['pdp'][$j->tanggal] += $j->total_pdp;
                    $data['positive'][$j->tanggal] += $j->total_positif;
                    $data['recovered'][$j->tanggal] += $j->total_sembuh;
                    $data['deceased'][$j->tanggal] += $j->total_meninggal;
                } else {
                    $data['odp'] += array($j->tanggal => $j->total_odp);
                    $data['pdp'] += array($j->tanggal => $j->total_pdp);
                    $data['positive'] += array($j->tanggal => $j->total_positif);
                    $data['recovered'] += array($j->tanggal => $j->total_sembuh);
                    $data['deceased'] += array($j->tanggal => $j->total_meninggal);
                }
            }
            $data['last_updated'] = strtotime(date('Y-m-d', strtotime($jatim[count($jatim) - 1]->tanggal))) . '000';
            // for ($k = 0; $k < count($db); $k++) {
            //     $epoch = substr($db[$k]['tanggal'], 0, 10);
            //     $dt = new DateTime("@$epoch");
            //     $db[$k]['tanggal'] = $dt->format('d M');
            // }
            // foreach ($db as $d) {
            //     $data['positive'] += array($d->tanggal => $d->total_positif);
            //     $data['recovered'] += array($d->tanggal => $d->total_sembuh);
            //     $data['deceased'] += array($d->tanggal => $d->total_meninggal);
            // }
            $data['district'] = array();
            $cnt = Jatim::groupBy('kode_kab')->get(['kode_kab']);
            $cnt = count($cnt);
            $db = Jatim::groupBy('kode_kab')->orderBy('nama_kab')->take($cnt)->distinct('kode_kab')->get(['nama_kab', 'kode_kab', 'total_odp', 'total_pdp', 'total_positif', 'total_sembuh', 'total_meninggal', 'tanggal']);
            foreach ($db as $d) {
                array_push($data['district'], array(
                    "name" => $d->nama_kab,
                    "district_code" => $d->kode_kab,
                    "total_odp" => $d->total_odp,
                    "total_pdp" => $d->total_pdp,
                    "total_positive" => $d->total_positif,
                    "total_recovered" => $d->total_sembuh,
                    "total_deceased" => $d->total_meninggal,
                    "last_update" => strtotime(date('Y-m-d', strtotime($d->tanggal)))
                ));
            }
        } else {
            $db = Jatim::whereKode_kab($district)->orderBy('tanggal', 'asc')->get();
            $data['name'] = $db[0]->nama_kab;
            foreach ($db as $d) {
                $data['odp'] += array(date('d M', strtotime($d->tanggal)) => $d->total_odp);
                $data['pdp'] += array(date('d M', strtotime($d->tanggal)) => $d->total_pdp);
                $data['positive'] += array(date('d M', strtotime($d->tanggal)) => $d->total_positif);
                $data['recovered'] += array(date('d M', strtotime($d->tanggal)) => $d->total_sembuh);
                $data['deceased'] += array(date('d M', strtotime($d->tanggal)) => $d->total_meninggal);
            }
            $data['last_updated'] = strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal)));
        }
        return response()->json($data);
    }

    public function jawa_barat(Request $request)
    {
        $district = $request->district;
        $sub_dist = $request->sub_dist;
        $data = array();
        if ($district == "-2" && $sub_dist == "-2") {
            $data = json_decode(jabar_daily::whereId("2")->get()[0]->data);
            $data = json_decode(json_encode($data), true);
        } else if ($district == "-1" && $sub_dist == "-1") {
            $data = json_decode(jabar_daily::whereId("1")->get()[0]->data);
            $data = json_decode(json_encode($data), true);
        } else if ($district != "-1" && $sub_dist == "-1") {
            $db = jabarprovsebaran::where('kode_kab', $district)->orderBy('tanggal_update', 'asc')->get();
            $data['name'] = jabarprovsebaran::where('kode_kab', $district)->take(1)->pluck('nama_kab')[0];
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['positive'] = array(
                "recovered" => 0,
                "deceased" => 0,
                "in_care" => 0
            );
            for ($z = count($db) - 1; $z >= 0; $z--) {
                if ($db[$z]->tanggal_update == "") {
                    continue;
                }
                $data['last_updated'] = $db[$z]['tanggal_update'];
                break;
            }
            $data['sub_dist'] = array();

            $pertama = true;
            $kedua = true;
            $idx_null = 0;
            foreach ($db as $d) {
                if ($d->status == 'ODP') {
                    if ($d->stage == 'Selesai') {
                        $data['odp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['odp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['odp']['deceased']++;
                    }
                } else if ($d->status == 'PDP') {
                    if ($d->stage == 'Selesai') {
                        $data['pdp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['pdp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['pdp']['deceased']++;
                    }
                } else if ($d->status == 'Positif') {
                    if ($d->stage == 'Sembuh') {
                        $data['positive']['recovered']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['positive']['deceased']++;
                    } else if ($d->stage == 'Proses') {
                        $data['positive']['in_care']++;
                    }
                }


                if ($pertama) {
                    array_push($data['sub_dist'], array(
                        "name" => $d->nama_kec,
                        // "name" => $d->nama_kel == "" ? "Data tanpa kecamatan" : $d->nama_kec,
                        // "sub_code" => $d->nama_kel == "" ? 0 : $d->kode_kec,
                        "sub_code" => $d->kode_kec,
                        "total_odp" => $d->status == 'ODP' ? 1 : 0,
                        "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                        "total_positive" => $d->status == 'Positif' ? 1 : 0,
                        "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                        "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                        "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                    ));
                    if ($d->nama_kel == "") {
                        $idx_null = 0;
                    }
                    $pertama = false;
                } else {
                    for ($i = 0; $i < count($data['sub_dist']); $i++) {
                        if (strcasecmp($data['sub_dist'][$i]['name'], $d->nama_kec) == 0) {
                            $d->status == 'ODP' ? $data['sub_dist'][$i]['total_odp']++ : '';
                            $d->status == 'PDP' ? $data['sub_dist'][$i]['total_pdp']++ : '';
                            $d->status == 'Positif' ? $data['sub_dist'][$i]['total_positive']++ : '';
                            $d->status == 'Positif' && $d->stage == 'Sembuh' ? $data['sub_dist'][$i]['total_recovered']++ : '';
                            $d->stage == 'Meninggal' ? $data['sub_dist'][$i]['total_deceased']++ : 0;
                            $kedua = true;
                            break;
                        } else {
                            $kedua = false;
                        }
                    }

                    if (!$kedua) {
                        $kedua = true;
                        array_push($data['sub_dist'], array(
                            "name" => $d->nama_kec,
                            // "name" => $d->nama_kel == "" ? "Data tanpa kecamatan" : $d->nama_kec,
                            "sub_code" => $d->kode_kec,
                            // "sub_code" => $d->nama_kel == "" ? 0 : $d->kode_kec,
                            "total_odp" => $d->status == 'ODP' ? 1 : 0,
                            "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                            "total_positive" => $d->status == 'Positif' ? 1 : 0,
                            "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                            "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                            "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                        ));
                        if ($d->nama_kel == "") {
                            $idx_null = count($data['sub_dist']) - 1;
                        }
                    }
                }
            }
            $data['sub_dist'][$idx_null]['name'] = "Data tanpa kecamatan";
            $data['sub_dist'][$idx_null]['sub_code'] = 0;
        } else if ($district != "-1" && $sub_dist != "-1") {
            $db = jabarprovsebaran::where('kode_kab', $district)->where('kode_kec', $sub_dist)->orderBy('tanggal_konfirmasi')->get();
            // return $db[0]->tanggal_update;
            $data['name'] = $db[0]->nama_kec;
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['positive'] = array(
                "recovered" => 0,
                "deceased" => 0,
                "in_care" => 0
            );
            for ($z = count($db) - 1; $z >= 0; $z--) {
                if ($db[$z]->tanggal_update == "") {
                    continue;
                }
                $data['last_updated'] = $db[$z]->tanggal_update;
                break;
            }
            $data['people'] = array();
            foreach ($db as $d) {
                if ($d->status == 'ODP') {
                    if ($d->stage == 'Selesai') {
                        $data['odp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['odp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['odp']['deceased']++;
                    }
                } else if ($d->status == 'PDP') {
                    if ($d->stage == 'Selesai') {
                        $data['pdp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['pdp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['pdp']['deceased']++;
                    }
                } else if ($d->status == 'Positif') {
                    if ($d->stage == 'Sembuh') {
                        $data['positive']['recovered']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['positive']['deceased']++;
                    } else if ($d->stage == 'Proses') {
                        $data['positive']['in_care']++;
                    }
                }
                array_push($data['people'], array(
                    "name" => $d->nama_kel,
                    "status" => $d->status,
                    "stage" => $d->stage,
                    "age" => $d->umur,
                    "gender" => $d->gender,
                    "lat" => $d->latitude,
                    "lng" => $d->longitude,
                    "confirmed_date" => $d->tanggal_konfirmasi,
                    "updated_at" => $d->tanggal_update
                ));
            }
        }

        return response()->json($data);
    }

    public function bantenAll(Request $request)
    {
        $district = $request->district;
        if ($district == "-1") {
            $db = banten::all();
            $data = array(
                "name" => "Banten",
                "odp" => [],
                "pdp" => [],
                "positive" => [],
                "in_care" => [],
                "recovered" => [],
                "deceased" => [],
                "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal))) . '000',
                "districts" => []
            );
            foreach ($db as $d) {
                $tgl = date('d M', strtotime($d->tanggal));
                $data['odp'] += array(
                    $tgl => [
                        "total" => $d->total_odp,
                        "process" => $d->odp_proses,
                        "finish" => $d->odp_selesai
                    ]
                );
                $data['pdp'] += array(
                    $tgl => [
                        "total" => $d->total_pdp,
                        "deceased" => $d->pdp_meninggal,
                        "in_care" => $d->pdp_rawat,
                        "recovered" => $d->pdp_sembuh
                    ]
                );
                $data['positive'] += array(
                    $tgl => [
                        "total" => $d->total_positif,
                        "deceased" => $d->pos_meninggal,
                        "in_care" => $d->pos_rawat,
                        "recovered" => $d->pos_sembuh
                    ]
                );
                $data['in_care'] += array(
                    $tgl => $d->total_dirawat
                );
                $data['recovered'] += array(
                    $tgl => $d->total_sembuh
                );
                $data['deceased'] += array(
                    $tgl => $d->total_meninggal
                );
            }
            $arr = banten_kabko::groupBy('wilayah')->orderBy('wilayah')->pluck('wilayah');
            $cnt = count($arr);
            $db = banten_kabko::whereIn('wilayah', $arr)->take($cnt)->orderBy('tanggal', 'desc')->distinct('wilayah')->get()->toArray();

            usort($db, function ($first, $second) {
                return strtolower($first['wilayah']) > strtolower($second['wilayah']);
            });

            foreach ($db as $d) {
                array_push($data['districts'], array(
                    "name" => $d['wilayah'],
                    "odp" => $d['total_odp'],
                    "pdp" => $d['total_pdp'],
                    "positive" => $d['total_positif'],
                    "recovered" => $d['total_sembuh'],
                    "deceased" => $d['total_meninggal'],
                    "last_update" => strtotime(date('Y-m-d', strtotime($d['tanggal'])))
                ));
            }
        } else {
            $db = banten_kabko::where('wilayah', $district)->orderBy('tanggal')->get();
            $data = array(
                "name" => $district,
                "odp" => [],
                "pdp" => [],
                "positive" => [],
                "recovered" => [],
                "deceased" => [],
                "last_updated" => strtotime(date('Y-m-d', strtotime($db[count($db) - 1]->tanggal))) . '000'
            );
            foreach ($db as $d) {
                $tgl = date('d M', strtotime($d->tanggal));
                $data['odp'] += array(
                    $tgl => $d->total_odp
                );
                $data['pdp'] += array(
                    $tgl => $d->total_pdp
                );
                $data['positive'] += array(
                    $tgl => $d->total_positif
                );
                $data['recovered'] += array(
                    $tgl => $d->total_sembuh
                );
                $data['deceased'] += array(
                    $tgl => $d->total_meninggal
                );
            }
        }
        return response()->json($data);
    }
}
