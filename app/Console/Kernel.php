<?php

namespace App\Console;

use App\Models\jabar;
use App\Models\jabar_daily;
use App\Models\jabarprovsebaran;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use App\Models\user;
use Illuminate\Support\Facades\DB;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $data = array();
            $data['name'] = "Jawa Barat";
            $db = jabarprovsebaran::orderBy('tanggal_update', 'asc')->get();
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['positive'] = array(
                "recovered" => 0,
                "deceased" => 0,
                "in_care" => 0
            );

            for ($z = count($db) - 1; $z >= 0; $z--) {
                if ($db[$z]->tanggal_update == "") {
                    continue;
                }
                $data['last_updated'] = $db[$z]['tanggal_update'];
                break;
            }

            $data['districts'] = array();
            $pertama = true;
            $kedua = true;
            $idx_null = 0;

            foreach ($db as $d) {
                if ($d->status == 'ODP') {
                    if ($d->stage == 'Selesai') {
                        $data['odp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['odp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['odp']['deceased']++;
                    }
                } else if ($d->status == 'PDP') {
                    if ($d->stage == 'Selesai') {
                        $data['pdp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['pdp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['pdp']['deceased']++;
                    }
                } else if ($d->status == 'Positif') {
                    if ($d->stage == 'Sembuh') {
                        $data['positive']['recovered']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['positive']['deceased']++;
                    } else if ($d->stage == 'Proses') {
                        $data['positive']['in_care']++;
                    }
                }

                if ($pertama) {
                    array_push($data['districts'], array(
                        "name" => $d->nama_kab,
                        "sub_code" => $d->kode_kab,
                        "total_odp" => $d->status == 'ODP' ? 1 : 0,
                        "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                        "total_positive" => $d->status == 'Positif' ? 1 : 0,
                        "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                        "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                        "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                    ));
                    $pertama = false;
                } else {
                    for ($i = 0; $i < count($data['districts']); $i++) {
                        if (strcasecmp($data['districts'][$i]['sub_code'], $d->kode_kab) == 0) {
                            $d->status == 'ODP' ? $data['districts'][$i]['total_odp']++ : '';
                            $d->status == 'PDP' ? $data['districts'][$i]['total_pdp']++ : '';
                            $d->status == 'Positif' ? $data['districts'][$i]['total_positive']++ : '';
                            $d->status == 'Positif' && $d->stage == 'Sembuh' ? $data['districts'][$i]['total_recovered']++ : '';
                            $d->stage == 'Meninggal' ? $data['districts'][$i]['total_deceased']++ : 0;
                            $d->tanggal_update == "" ? $data['districts'][$i]['last_updated'] = $d->tanggal_konfirmasi : $data['districts'][$i]['last_updated'] = $d->tanggal_update;
                            $kedua = true;
                            break;
                        } else {
                            $kedua = false;
                        }
                    }

                    if (!$kedua) {
                        $kedua = true;
                        array_push($data['districts'], array(
                            "name" => $d->nama_kab,
                            "sub_code" => $d->kode_kab,
                            "total_odp" => $d->status == 'ODP' ? 1 : 0,
                            "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                            "total_positive" => $d->status == 'Positif' ? 1 : 0,
                            "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                            "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                            "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                        ));
                    }
                }
            }

            usort($data['districts'], function ($first, $second) {
                return strtolower($first['name']) > strtolower($second['name']);
            });
            $j = jabar_daily::find(1);
            $j->data = json_encode($data);
            $j->save();

            //ENDPOINT JABAR BY JABARPROV TABLE
            $data = array();
            $data['name'] = "Jawa Barat";
            $db = jabar::orderBy('tanggal')->get();
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "total" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "total" => 0
            );

            $data['positive'] = 0;
            $data['recovered'] = 0;
            $data['deceased'] = 0;
            $data['last_updated'] = strtotime(date('Y-m-d', strtotime($db[count($db) - 1]['tanggal']))) . "000";
            foreach ($db as $d) {
                $data['odp']['finished'] += $d['odp_selesai'];
                $data['odp']['progress'] += $d['odp_proses'];
                $data['odp']['total'] += $d['total_odp'];
                $data['pdp']['finished'] += $d['pdp_selesai'];
                $data['pdp']['progress'] += $d['pdp_proses'];
                $data['pdp']['total'] += $d['total_pdp'];
                $data['positive'] += $d['total_positif'];
                $data['recovered'] += $d['total_sembuh'];
                $data['deceased'] += $d['total_meninggal'];
            }
            $db = jabarprovsebaran::orderBy('tanggal_update', 'asc')->get();
            $data['districts'] = array();
            $pertama = true;
            $kedua = true;
            $idx_null = 0;

            foreach ($db as $d) {
                if ($pertama) {
                    array_push($data['districts'], array(
                        "name" => $d->nama_kab,
                        "sub_code" => $d->kode_kab,
                        "total_odp" => $d->status == 'ODP' ? 1 : 0,
                        "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                        "total_positive" => $d->status == 'Positif' ? 1 : 0,
                        "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                        "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                        "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                    ));
                    $pertama = false;
                } else {
                    for ($i = 0; $i < count($data['districts']); $i++) {
                        if (strcasecmp($data['districts'][$i]['sub_code'], $d->kode_kab) == 0) {
                            $d->status == 'ODP' ? $data['districts'][$i]['total_odp']++ : '';
                            $d->status == 'PDP' ? $data['districts'][$i]['total_pdp']++ : '';
                            $d->status == 'Positif' ? $data['districts'][$i]['total_positive']++ : '';
                            $d->status == 'Positif' && $d->stage == 'Sembuh' ? $data['districts'][$i]['total_recovered']++ : '';
                            $d->stage == 'Meninggal' ? $data['districts'][$i]['total_deceased']++ : 0;
                            $d->tanggal_update == "" ? $data['districts'][$i]['last_updated'] = $d->tanggal_konfirmasi : $data['districts'][$i]['last_updated'] = $d->tanggal_update;
                            $kedua = true;
                            break;
                        } else {
                            $kedua = false;
                        }
                    }

                    if (!$kedua) {
                        $kedua = true;
                        array_push($data['districts'], array(
                            "name" => $d->nama_kab,
                            "sub_code" => $d->kode_kab,
                            "total_odp" => $d->status == 'ODP' ? 1 : 0,
                            "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                            "total_positive" => $d->status == 'Positif' ? 1 : 0,
                            "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                            "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                            "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                        ));
                    }
                }
            }

            usort($data['districts'], function ($first, $second) {
                return strtolower($first['name']) > strtolower($second['name']);
            });
            $j = jabar_daily::find(2);
            $j->data = json_encode($data);
            $j->save();
        })->between('05:50', '06:30');

        $schedule->call(function () {
            $data = array();
            $data['name'] = "Jawa Barat";
            $db = jabarprovsebaran::orderBy('tanggal_update', 'asc')->get();
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "deceased" => 0
            );
            $data['positive'] = array(
                "recovered" => 0,
                "deceased" => 0,
                "in_care" => 0
            );

            for ($z = count($db) - 1; $z >= 0; $z--) {
                if ($db[$z]->tanggal_update == "") {
                    continue;
                }
                $data['last_updated'] = $db[$z]['tanggal_update'];
                break;
            }

            $data['districts'] = array();
            $pertama = true;
            $kedua = true;
            $idx_null = 0;

            foreach ($db as $d) {
                if ($d->status == 'ODP') {
                    if ($d->stage == 'Selesai') {
                        $data['odp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['odp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['odp']['deceased']++;
                    }
                } else if ($d->status == 'PDP') {
                    if ($d->stage == 'Selesai') {
                        $data['pdp']['finished']++;
                    } else if ($d->stage == 'Proses') {
                        $data['pdp']['progress']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['pdp']['deceased']++;
                    }
                } else if ($d->status == 'Positif') {
                    if ($d->stage == 'Sembuh') {
                        $data['positive']['recovered']++;
                    } else if ($d->stage == 'Meninggal') {
                        $data['positive']['deceased']++;
                    } else if ($d->stage == 'Proses') {
                        $data['positive']['in_care']++;
                    }
                }

                if ($pertama) {
                    array_push($data['districts'], array(
                        "name" => $d->nama_kab,
                        "sub_code" => $d->kode_kab,
                        "total_odp" => $d->status == 'ODP' ? 1 : 0,
                        "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                        "total_positive" => $d->status == 'Positif' ? 1 : 0,
                        "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                        "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                        "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                    ));
                    $pertama = false;
                } else {
                    for ($i = 0; $i < count($data['districts']); $i++) {
                        if (strcasecmp($data['districts'][$i]['sub_code'], $d->kode_kab) == 0) {
                            $d->status == 'ODP' ? $data['districts'][$i]['total_odp']++ : '';
                            $d->status == 'PDP' ? $data['districts'][$i]['total_pdp']++ : '';
                            $d->status == 'Positif' ? $data['districts'][$i]['total_positive']++ : '';
                            $d->status == 'Positif' && $d->stage == 'Sembuh' ? $data['districts'][$i]['total_recovered']++ : '';
                            $d->stage == 'Meninggal' ? $data['districts'][$i]['total_deceased']++ : 0;
                            $d->tanggal_update == "" ? $data['districts'][$i]['last_updated'] = $d->tanggal_konfirmasi : $data['districts'][$i]['last_updated'] = $d->tanggal_update;
                            $kedua = true;
                            break;
                        } else {
                            $kedua = false;
                        }
                    }

                    if (!$kedua) {
                        $kedua = true;
                        array_push($data['districts'], array(
                            "name" => $d->nama_kab,
                            "sub_code" => $d->kode_kab,
                            "total_odp" => $d->status == 'ODP' ? 1 : 0,
                            "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                            "total_positive" => $d->status == 'Positif' ? 1 : 0,
                            "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                            "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                            "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                        ));
                    }
                }
            }

            usort($data['districts'], function ($first, $second) {
                return strtolower($first['name']) > strtolower($second['name']);
            });
            $j = jabar_daily::find(1);
            $j->data = json_encode($data);
            $j->save();

            //ENDPOINT JABAR BY JABARPROV TABLE
            $data = array();
            $data['name'] = "Jawa Barat";
            $db = jabar::orderBy('tanggal')->get();
            $data['odp'] = array(
                "finished" => 0,
                "progress" => 0,
                "total" => 0
            );
            $data['pdp'] = array(
                "finished" => 0,
                "progress" => 0,
                "total" => 0
            );

            $data['positive'] = 0;
            $data['recovered'] = 0;
            $data['deceased'] = 0;
            $data['last_updated'] = strtotime(date('Y-m-d', strtotime($db[count($db) - 1]['tanggal']))) . "000";
            foreach ($db as $d) {
                $data['odp']['finished'] += $d['odp_selesai'];
                $data['odp']['progress'] += $d['odp_proses'];
                $data['odp']['total'] += $d['total_odp'];
                $data['pdp']['finished'] += $d['pdp_selesai'];
                $data['pdp']['progress'] += $d['pdp_proses'];
                $data['pdp']['total'] += $d['total_pdp'];
                $data['positive'] += $d['total_positif'];
                $data['recovered'] += $d['total_sembuh'];
                $data['deceased'] += $d['total_meninggal'];
            }
            $db = jabarprovsebaran::orderBy('tanggal_update', 'asc')->get();
            $data['districts'] = array();
            $pertama = true;
            $kedua = true;
            $idx_null = 0;

            foreach ($db as $d) {
                if ($pertama) {
                    array_push($data['districts'], array(
                        "name" => $d->nama_kab,
                        "sub_code" => $d->kode_kab,
                        "total_odp" => $d->status == 'ODP' ? 1 : 0,
                        "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                        "total_positive" => $d->status == 'Positif' ? 1 : 0,
                        "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                        "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                        "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                    ));
                    $pertama = false;
                } else {
                    for ($i = 0; $i < count($data['districts']); $i++) {
                        if (strcasecmp($data['districts'][$i]['sub_code'], $d->kode_kab) == 0) {
                            $d->status == 'ODP' ? $data['districts'][$i]['total_odp']++ : '';
                            $d->status == 'PDP' ? $data['districts'][$i]['total_pdp']++ : '';
                            $d->status == 'Positif' ? $data['districts'][$i]['total_positive']++ : '';
                            $d->status == 'Positif' && $d->stage == 'Sembuh' ? $data['districts'][$i]['total_recovered']++ : '';
                            $d->stage == 'Meninggal' ? $data['districts'][$i]['total_deceased']++ : 0;
                            $d->tanggal_update == "" ? $data['districts'][$i]['last_updated'] = $d->tanggal_konfirmasi : $data['districts'][$i]['last_updated'] = $d->tanggal_update;
                            $kedua = true;
                            break;
                        } else {
                            $kedua = false;
                        }
                    }

                    if (!$kedua) {
                        $kedua = true;
                        array_push($data['districts'], array(
                            "name" => $d->nama_kab,
                            "sub_code" => $d->kode_kab,
                            "total_odp" => $d->status == 'ODP' ? 1 : 0,
                            "total_pdp" => $d->status == 'PDP' ? 1 : 0,
                            "total_positive" => $d->status == 'Positif' ? 1 : 0,
                            "total_recovered" => $d->status == 'Positif' && $d->stage == 'Sembuh' ? 1 : 0,
                            "total_deceased" => $d->stage == 'Meninggal' ? 1 : 0,
                            "last_updated" => $d->tanggal_update == "" ? $d->tanggal_konfirmasi : $d->tanggal_update
                        ));
                    }
                }
            }

            usort($data['districts'], function ($first, $second) {
                return strtolower($first['name']) > strtolower($second['name']);
            });
            $j = jabar_daily::find(2);
            $j->data = json_encode($data);
            $j->save();
        })->between('17:50', '18:30');

        $schedule->call(function () {
            $messaging = app('firebase.messaging');
            $deviceTokens = user::pluck('app_token')->toArray();
            $message = CloudMessage::new();
            $notification = Notification::fromArray([
                'title' => 'Update Data Covid-19 Indonesia Terbaru'
            ]);
            $message = $message->withNotification($notification);
            $report = $messaging->sendMulticast($message, $deviceTokens);
        })->between('09:50', '10:30');

        $schedule->call(function () {
            $messaging = app('firebase.messaging');
            $deviceTokens = user::pluck('app_token')->toArray();
            $message = CloudMessage::new();
            $notification = Notification::fromArray([
                'title' => 'Update Data Covid-19 Indonesia Terbaru'
            ]);
            $message = $message->withNotification($notification);
            $report = $messaging->sendMulticast($message, $deviceTokens);
        })->between('19:50', '20:30');

        // $schedule->call(function () {
        //     $str = 'ini jam : ' . date('D M Y h:i:s');
        //     $messaging = app('firebase.messaging');
        //     $deviceTokens = ['cdXB4bmLQB2RLehp8Vu20O:APA91bEYReI1cM8bMpWboOzL99MHQvMpolJyTTrntRtp2Vtow0MSeJH_dtTCoRFAYU_xqP-WA6amR8Xc_9fTwoHRSty4P35lyydDSY1247MPasiwU9tAXXMhHHqUf0OsRhbIy2heSt1C'];
        //     $message = CloudMessage::new();
        //     $notification = Notification::fromArray([
        //         'title' => $str
        //     ]);
        //     $message = $message->withNotification($notification);
        //     $report = $messaging->sendMulticast($message, $deviceTokens);
        // })->everyMinute();
        // })->between('18:00', '18:59');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
